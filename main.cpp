#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QZXing.h>
#include <src/restful.h>
#include <src/usuario.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QZXing::registerQMLTypes();

    Restful rest;
    Usuario usuario;

    QQmlApplicationEngine engine;

    QPM_INIT(engine);


    engine.rootContext()->setContextProperty("usuario", &usuario);
    engine.rootContext()->setContextProperty("restLogin", rest.getEndpointLogin());
    engine.rootContext()->setContextProperty("restRegistro", rest.getEndpointRegistro());
    engine.rootContext()->setContextProperty("restBalance", rest.getEndpointBalance());
    engine.rootContext()->setContextProperty("restPago", rest.getEndpointPago());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
