import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import Material 0.3
import Material.ListItems 0.1 as ListItem

import "qrc:/controladores/ControladorSaldo.js" as ControladorSaldo

Page
{
    width: parent.width
    height: parent.height

    title: qsTr("Saldo")

    Component.onCompleted: ControladorSaldo.consultar()

    Connections
    {
        target: restBalance
        onCambioEstatusRespuesta: ControladorSaldo.recibirRespuestaConsulta()
    }


    Text
    {
        id: saldo
        text: qsTr("Coin")
        font.pointSize: 30
        anchors.centerIn: parent
    }

//    Button
//    {
//        id:botonIniciar
//        text: qsTr("Recargar")
//        backgroundColor: tema.primaryColor
//        onClicked: ControladorSaldo.recargar()
//        anchors.top: contrasena.bottom
//        anchors.topMargin: 20
//        anchors.horizontalCenter: parent.horizontalCenter
//        z: 2
//    }


}
