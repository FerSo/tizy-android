import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2

import Material 0.3
import Material.ListItems 0.1 as ListItem

import "qrc:/controladores/ControladorLogin.js" as Controlador

Page
{
    id: login
    actionBar.hidden: true
    width: parent.width
    height: parent.height


    Connections
    {
        target: restLogin
        onCambioEstatusRespuesta: Controlador.recibirRespuesta()
    }

    Image
    {
        id: logo
        source: "qrc:/imagenes/logo"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: formularioLogin.top
        anchors.bottomMargin: 20
        width: parent.width*0.2
        height: width
        z: 2
    }

    TextField
    {
        id: cedula
        placeholderText: qsTr("cedula")
        floatingLabel: true
        font.pointSize: 14
        anchors.top: logo.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        z: 2
    }

    TextField
    {
        id: contrasena
        placeholderText: qsTr("Contrasena")
        echoMode: TextInput.Password
        floatingLabel: true
        font.pointSize: 14
        anchors.top: cedula.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        z: 2
    }

    Button
    {
        id:botonIniciar
        text: qsTr("Iniciar")
        backgroundColor: tema.primaryColor
        onClicked: Controlador.login()
        anchors.top: contrasena.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        z: 2
    }

    Button
    {
        id:botonRegistro
        text: qsTr("Registrarse")
        backgroundColor: tema.primaryColor
        onClicked: pageStack.push("qrc:/registro");
        anchors.top: botonIniciar.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
    }



    Image
    {
        id: fondo
        anchors.fill: parent
        source: "qrc:/imagenes/fondo"
    }

}
