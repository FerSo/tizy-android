import QtQuick 2.9
import QtQuick.Window 2.2

import Material 0.3
import Material.ListItems 0.1 as ListItem

ApplicationWindow
{
    id: app
    width: 720
    height: 1024

    visible: true

    initialPage: "qrc:/login"

    theme
    {
        id: tema
        primaryColor: "#3F51B5"
        accentColor: "#EEF9A0"
        backgroundColor: "#ffffff"
    }

    Snackbar
    {
        id: alerta
        duration: 7000
    }
}
