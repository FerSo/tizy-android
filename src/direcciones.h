#ifndef DIRECCIONES_H
#define DIRECCIONES_H

#include <QObject>

class Direcciones : public QObject
{
    Q_OBJECT
public:
    explicit Direcciones(QObject *parent = nullptr);

    QString login;
    QString registro;
    QString perfil;
    QString qr;
    QString balance;

signals:

public slots:
};

#endif // DIRECCIONES_H
